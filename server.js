const bodyParser = require('body-parser');
const express = require('express');
const app = express();

app.use(express.static('.') );
// Qualquer REQuisição ou submit de formulario que chegar no servidor, esse cara vai transformar em Objeto
app.use(bodyParser.urlencoded({ extended: true}));
// Se vir um Json no corpo da requisição, vai converter em Objeto
app.use(bodyParser.json() );

app.get('/teste', (req,res) => res.send('Foi'))
app.listen(8080, () => console.log('Executando...'));